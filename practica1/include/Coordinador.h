#include "Mundo.h"

#include <fstream>
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


class Coordinador
{
public:
	Coordinador();
	virtual ~Coordinador();

	void tecla (unsigned char key, int x, int y);
	void dibuja();
	void mueve(int value);
	void Init (void);

protected:
	CMundo mundo;
	enum Estado {JUEGO, VICTORIA};
	Estado estado;

};
