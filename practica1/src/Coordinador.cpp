#include "Coordinador.h"



Coordinador::Coordinador()
{
	Init();
	
}

Coordinador::~Coordinador()
{

}

void Coordinador::dibuja()
{
	if (estado==JUEGO)
		mundo.OnDraw();
	else if (estado==VICTORIA) {
		//Borrado de la pantalla	
   		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		

		//Para definir el punto de vista
		glMatrixMode(GL_MODELVIEW);	
		glLoadIdentity();
	
		gluLookAt(0.0, 0, 17,  // posicion del ojo
			0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
			0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	if (mundo.puntos1==3)
		sprintf(cad,"GANA EL JUGADOR 1  Presione 'e' para salir");
	else if (mundo.puntos2==3)
		sprintf(cad,"GANA EL JUGADOR 2  Presione 'e' para salir");
	mundo.print(cad,10,0,1,1,1);

	}

	glutSwapBuffers();

}

void Coordinador::tecla (unsigned char key, int x, int y)
{
	if (estado==JUEGO)
		mundo.OnKeyboardDown(key,x,y);
	else if (estado==VICTORIA)
	{
		if (key=='e')
			exit(0);
	}

}

void Coordinador::mueve(int value)
{
	if (estado==JUEGO) {
		mundo.OnTimer(value);
		if ((mundo.puntos1==3)||(mundo.puntos2==3))
			estado=VICTORIA;
	}

}

void Coordinador::Init (void)
{
	estado=JUEGO;
	mundo.Init();
	mundo.InitGL();

}

