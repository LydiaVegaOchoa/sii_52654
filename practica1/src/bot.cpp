#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include <stdio.h>
#include <stdlib.h>

#include "DatosMemCompartida.h"




int main (int argc, char **argv)
{
	DatosMemCompartida* datos_p;
	char* aux_p;
	int fd_comp;


	/* Abrir el fichero */	
	if ((fd_comp=open("/tmp/fichero.txt", O_RDWR))<0) {
		perror ("No se puede abrir o crear el fichero a proyectar");
		return (1);
	}

	/* Proyectarlo en memoria */
	if ((aux_p= (char *) mmap(NULL, sizeof(datos_p), PROT_READ|PROT_WRITE, MAP_SHARED,
			fd_comp, 0))==MAP_FAILED){
                perror("Error en la proyección del fichero");
                close(fd_comp);
                return(1);
        }


	/* Cerrar el descriptor de fichero */
	close (fd_comp);

	/* Asignar la dirección de memoria */
	datos_p = (DatosMemCompartida*) aux_p;

	/* Bucle de acción del bot */
		
	while (1) {

		if (datos_p->raqueta1.y2 < datos_p->esfera.centro.y)
			datos_p->accion=1;
		else if (datos_p->raqueta1.y1 > datos_p->esfera.centro.y)
				datos_p->accion=-1;
		else { datos_p->accion=0; usleep(25000);}
		

	}


	return(0);
}

