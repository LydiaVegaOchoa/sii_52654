#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>



int main (int argc, char **argv)
{
	int fd;
	char m[100];

	

	

	/* Comprobación de los datos introducidos por el usuario */
	if (argc!=1) {
		fprintf (stderr, "Demasiados argumentos para la ejecución\n");
		return (1);
	}


	/* Creación del FIFO */
	if (mkfifo ("/tmp/fifo", 0600)<0) { /* Permisos w-r para el dueño */
		perror ("No se puede crear la tuberia");
		return (1);
	}


	/* Apertura del FIFO */
	if ((fd=open("/tmp/fifo", O_RDONLY))<0) {
		perror ("No se puede abrir la tuberia");
		return (1);
	}

	/* Funciones del FIFO */
	//buffer de lectura de datos cadena de caracteres
	while (read (fd, m, sizeof(m))==sizeof(m)) {
		write (1, m, sizeof(m));

	}


	/* Cierre y destrucción del FIFO */
	close (fd);
	unlink ("/tmp/fifo");

	return(0);


}
